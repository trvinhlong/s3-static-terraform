resource "aws_instance" "this" {
  ami                         = "ami-0310483fb2b488153"
  instance_type               = "t2.micro"
  vpc_security_group_ids      = [var.vpc_sg.web]
  associate_public_ip_address = true
  key_name                    = var.key_name
  user_data = file("${path.module}/userdata.tpl")

  user_data_replace_on_change = true
  tags = {
    Name = "${var.name}-ec2-webserver"
  }
}
